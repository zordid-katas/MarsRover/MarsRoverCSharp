﻿using Xunit;

namespace MarsRover
{
    public class RoverTest
    {
        private readonly Rover rover;

        public RoverTest()
        {
            rover = new Rover(1, 1, "NORTH");
        }

        [Fact]
        private void TestCreation()
        {
            Assert.Equal(1, rover.X);
            Assert.Equal(1, rover.Y);
        }

        [Fact]
        public void TestMoveForward()
        {
            rover.ProcessCommands("f");
            Assert.Equal(2, rover.Y);
            Assert.Equal(1, rover.X);
        }

        [Fact]
        public void TestMoveBackward()
        {
            rover.ProcessCommands("b");
            Assert.Equal(0, rover.Y);
            Assert.Equal(1, rover.X);
        }

        [Fact]
        public void TestMoveForwardTwice()
        {
            rover.ProcessCommands("ff");
            Assert.Equal(3, rover.Y);
            Assert.Equal(1, rover.X);
        }

        [Fact]
        public void TestMoveForwardAndThenBackwards()
        {
            rover.ProcessCommands("fb");
            Assert.Equal(1, rover.Y);
            Assert.Equal(1, rover.X);
        }

        [Fact]
        public void TestMoveForwardAndThenBackwardMultipleTimes()
        {
            rover.ProcessCommands("ffffbb");
            Assert.Equal(3, rover.Y);
            Assert.Equal(1, rover.X);
        }

        [Fact]
        public void TestTurnRightAndMoveForward()
        {
            rover.ProcessCommands("rf");
            Assert.Equal(1, rover.Y);
            Assert.Equal(2, rover.X);
            Assert.Equal("EAST", rover.Direction);
        }

        [Fact]
        public void TestMoving()
        {
            rover.ProcessCommands("ffrff");
            Assert.Equal(3, rover.Y);
            Assert.Equal(3, rover.X);
            Assert.Equal("EAST", rover.Direction);
        }

        [Fact]
        public void TestTurningLeftLeft()
        {
            rover.ProcessCommands("ffllb");
            Assert.Equal(4, rover.Y);
            Assert.Equal(1, rover.X);
            Assert.Equal("SOUTH", rover.Direction);
        }

        [Fact]
        public void TestTurningAround()
        {
            rover.ProcessCommands("ffllff");
            Assert.Equal(1, rover.Y);
            Assert.Equal(1, rover.X);
            Assert.Equal("SOUTH", rover.Direction);
        }


    }
}
